#!/bin/bash
#About this script

clear
echo -e "===================================================="
echo -e "Script Auto Install SSH & OpenVPN v2.4" 
echo -e "===================================================="
echo -e ""
echo -e "For Debian 8 32 bit & 64 bit"
echo -e "Only for VPS with KVM and VMWare virtualization"
echo -e ""
echo -e "Original script by :"
echo -e "* Fornesia"
echo -e "* Rzengineer"
echo -e "* Fawzya"
echo -e "* Aiman Amir"
echo -e ""
echo -e "Modified by CipherX"
echo -e ""
