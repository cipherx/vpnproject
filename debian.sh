#!/bin/bash
#
# Script by CipherX
# Thank : Aiman Amir
# ==================================================
# 

# install certificates
apt-get install ca-certificates

# initialisasi var
export DEBIAN_FRONTEND=noninteractive
OS=`uname -m`;
MYIP=$(wget -qO- ipv4.icanhazip.com);
MYIP2="s/xxxxxxxxx/$MYIP/g";

#regenerate hostkey
#rm -r /etc/ssh/*key*
dpkg-reconfigure openssh-server



# go to root
cd

#Extract tar
#tar xf install.tar
#path_install=/root/install

# disable ipv6
echo 1 > /proc/sys/net/ipv6/conf/all/disable_ipv6
sed -i '$ i\echo 1 > /proc/sys/net/ipv6/conf/all/disable_ipv6' /etc/rc.local

# install wget and curl
apt-get update;apt-get -y install wget curl;

# set time GMT +7
ln -fs /usr/share/zoneinfo/Asia/Bangkok /etc/localtime

# set locale
sed -i 's/AcceptEnv/#AcceptEnv/g' /etc/ssh/sshd_config
service ssh restart

# set repo
wget -O /etc/apt/sources.list "https://bitbucket.org/cipherx/vpnproject/raw/ac660a93b3871d99951d301e2f4c0891a25b54d5/sources.list.debian"
#rm /etc/apt/sources.list
#cp $path_install/sources.list.debian /etc/apt/sources.list
wget "http://www.dotdeb.org/dotdeb.gpg"
wget "http://www.webmin.com/jcameron-key.asc"
cat dotdeb.gpg | apt-key add -;rm dotdeb.gpg
cat jcameron-key.asc | apt-key add -;rm jcameron-key.asc

# update
apt-get update

# install webserver
apt-get -y install nginx php5-fpm php5-cli

# install essential package
#apt-get -y install bmon iftop htop nmap axel nano iptables traceroute sysv-rc-conf dnsutils bc nethogs openvpn vnstat less screen psmisc apt-file whois ptunnel ngrep mtr git zsh mrtg snmp snmpd snmp-mibs-downloader unzip rsyslog debsums rkhunter
apt-get -y install bmon iftop htop nmap axel nano iptables traceroute sysv-rc-conf dnsutils bc nethogs openvpn vnstat less screen psmisc apt-file whois ptunnel ngrep mtr git zsh snmp snmpd unzip rsyslog debsums rkhunter
apt-get -y install build-essential

# disable exim
service exim4 stop
sysv-rc-conf exim4 off

# update apt-file
apt-file update

# setting vnstat
vnstat -u -i eth0
service vnstat restart

# install figlet
apt-get install figlet
echo "clear" >> .bashrc
echo 'figlet -k "$HOSTNAME"' >> .bashrc
echo 'echo -e "Welcome to the server $HOSTNAME"' >> .bashrc
echo 'echo -e "Script mod by CipherX"' >> .bashrc
echo 'echo -e "Type the menu to display the list of available commands"' >> .bashrc
echo 'echo -e ""' >> .bashrc

# install webserver
cd
rm /etc/nginx/sites-enabled/default
rm /etc/nginx/sites-available/default
wget -O /etc/nginx/nginx.conf "https://bitbucket.org/cipherx/vpnproject/raw/ac660a93b3871d99951d301e2f4c0891a25b54d5/nginx.conf"
#mv /etc/nginx/nginx.conf /etc/nginx/nginx.bak
#cp $path_install/nginx.conf /etc/nginx/nginx.conf
mkdir -p /home/vps/public_html
echo "<pre>Setup by CipherX</pre>" > /home/vps/public_html/index.html
echo "<?php phpinfo(); ?>" > /home/vps/public_html/info.php
wget -O /etc/nginx/conf.d/vps.conf "https://bitbucket.org/cipherx/vpnproject/raw/ac660a93b3871d99951d301e2f4c0891a25b54d5/vps.conf"
#cp $path_install/vps.conf /etc/nginx/conf.d/vps.conf
sed -i 's/listen = \/var\/run\/php5-fpm.sock/listen = 127.0.0.1:9000/g' /etc/php5/fpm/pool.d/www.conf
service nginx restart

# install openvpn
wget -O /etc/openvpn/openvpn.tar "https://bitbucket.org/cipherx/vpnproject/raw/ac660a93b3871d99951d301e2f4c0891a25b54d5/openvpn-debian.tar"
cd /etc/openvpn
#mv $path_install/openvpn.tar openvpn.tar
tar xf openvpn.tar
rm openvpn.tar
wget -O /etc/openvpn/1194.conf "https://bitbucket.org/cipherx/vpnproject/raw/ac660a93b3871d99951d301e2f4c0891a25b54d5/1194.conf"
#cp $path_install/1194.conf /etc/openvpn/
service openvpn restart
sysctl -w net.ipv4.ip_forward=1
sed -i 's/#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/g' /etc/sysctl.conf
iptables -t nat -I POSTROUTING -s 192.168.100.0/24 -o eth0 -j MASQUERADE
iptables-save > /etc/iptables_yg_baru_dibikin.conf
wget -O /etc/network/if-up.d/iptables "https://bitbucket.org/cipherx/vpnproject/raw/ac660a93b3871d99951d301e2f4c0891a25b54d5/iptables"
#cp $path_install/iptables /etc/network/if-up.d/iptables
chmod +x /etc/network/if-up.d/iptables
service openvpn restart

# configuration openvpn client
cd /etc/openvpn/
wget -O /etc/openvpn/client.ovpn "https://bitbucket.org/cipherx/vpnproject/raw/ac660a93b3871d99951d301e2f4c0891a25b54d5/client-1194.conf"
#cp $path_install/client-1194.conf /etc/openvpn/client.ovpn
sed -i $MYIP2 /etc/openvpn/client.ovpn;
cp client.ovpn /home/vps/public_html/

cd
# install badvpn
wget -O /usr/bin/badvpn-udpgw "https://bitbucket.org/cipherx/vpnproject/raw/ac660a93b3871d99951d301e2f4c0891a25b54d5/badvpn-udpgw"
#cp $path_install/badvpn-udpgw /usr/bin/badvpn-udpgw
if [ "$OS" == "x86_64" ]; then
  wget -O /usr/bin/badvpn-udpgw "https://bitbucket.org/cipherx/vpnproject/raw/ac660a93b3871d99951d301e2f4c0891a25b54d5/badvpn-udpgw"
#  cp $path_install/badvpn-udpgw /usr/bin/badvpn-udpgw
fi
sed -i '$ i\screen -AmdS badvpn badvpn-udpgw --listen-addr 127.0.0.1:7300' /etc/rc.local
chmod +x /usr/bin/badvpn-udpgw
screen -AmdS badvpn badvpn-udpgw --listen-addr 127.0.0.1:7300
cd

# setting port ssh
sed -i 's/Port 22/Port 22/g' /etc/ssh/sshd_config
sed -i '/Port 22/a Port 143' /etc/ssh/sshd_config
service ssh restart

# install dropbear
apt-get -y install dropbear
sed -i 's/NO_START=1/NO_START=0/g' /etc/default/dropbear
sed -i 's/DROPBEAR_PORT=22/DROPBEAR_PORT=443/g' /etc/default/dropbear
sed -i 's/DROPBEAR_EXTRA_ARGS=/DROPBEAR_EXTRA_ARGS="-p 80 -p 443"/g' /etc/default/dropbear
echo "/bin/false" >> /etc/shells
echo "/usr/sbin/nologin" >> /etc/shells
service ssh restart
service dropbear restart
# install vnstat gui
cd /home/vps/public_html/
wget http://www.sqweek.com/sqweek/files/vnstat_php_frontend-1.5.1.tar.gz
tar xf vnstat_php_frontend-1.5.1.tar.gz
rm vnstat_php_frontend-1.5.1.tar.gz
mv vnstat_php_frontend-1.5.1 vnstat
cd vnstat
sed -i 's/eth0/venet0/g' config.php
sed -i "s/\$iface_list = array('eth0', 'sixxs');/\$iface_list = array('eth0');/g" config.php
sed -i "s/\$language = 'nl';/\$language = 'en';/g" config.php
sed -i 's/Internal/Internet/g' config.php
sed -i '/SixXS IPv6/d' config.php
cd

# install fail2ban
apt-get -y install fail2ban;service fail2ban restart

# install squid3
apt-get -y install squid3
wget -O /etc/squid3/squid.conf "https://bitbucket.org/cipherx/vpnproject/raw/ac660a93b3871d99951d301e2f4c0891a25b54d5/squid3.conf"
#cp $path_install/squid3.conf /etc/squid3/squid.conf
sed -i $MYIP2 /etc/squid3/squid.conf;
service squid3 restart

# install webmin
cd
wget http://prdownloads.sourceforge.net/webadmin/webmin_1.860_all.deb
dpkg -i --force-all webmin_1.860_all.deb;
apt-get -y -f install;
rm /root/webmin_1.860_all.deb
service webmin restart

# download script
cd /usr/bin
wget -O menu "https://bitbucket.org/cipherx/vpnproject/raw/ac660a93b3871d99951d301e2f4c0891a25b54d5/menu.sh"
wget -O usernew "https://bitbucket.org/cipherx/vpnproject/raw/ac660a93b3871d99951d301e2f4c0891a25b54d5/usernew.sh"
wget -O trial-user "https://bitbucket.org/cipherx/vpnproject/raw/ac660a93b3871d99951d301e2f4c0891a25b54d5/trial.sh"
wget -O del-user "https://bitbucket.org/cipherx/vpnproject/raw/ac660a93b3871d99951d301e2f4c0891a25b54d5/hapus.sh"
wget -O login "https://bitbucket.org/cipherx/vpnproject/raw/ac660a93b3871d99951d301e2f4c0891a25b54d5/user-login.sh"
wget -O member "https://bitbucket.org/cipherx/vpnproject/raw/ac660a93b3871d99951d301e2f4c0891a25b54d5/user-list.sh"
wget -O resvis "https://bitbucket.org/cipherx/vpnproject/raw/ac660a93b3871d99951d301e2f4c0891a25b54d5/resvis.sh"
wget -O speedtest "https://bitbucket.org/cipherx/vpnproject/raw/ac660a93b3871d99951d301e2f4c0891a25b54d5/speedtest_cli.py"
wget -O about "https://bitbucket.org/cipherx/vpnproject/raw/ac660a93b3871d99951d301e2f4c0891a25b54d5/about.sh"

#cp $path_install/menu.sh menu
#cp $path_install/usernew.sh usernew
#cp $path_install/trial.sh trial-user
#cp $path_install/hapus.sh del-user
#cp $path_install/user-login.sh login
#cp $path_install/user-list.sh member
#cp $path_install/resvis.sh resvis
#cp $path_install/speedtest_cli.py speedtest
#cp $path_install/about.sh about

echo "0 0 * * * root /usr/bin/reboot" > /etc/cron.d/reboot
echo "* * * * * service dropbear restart" > /etc/cron.d/dropbear
chmod +x menu
chmod +x usernew
chmod +x trial-user
chmod +x del-user
chmod +x login
chmod +x member
chmod +x resvis
chmod +x speedtest
chmod +x about

# finishing
cd
chown -R www-data:www-data /home/vps/public_html
service nginx start
service vnstat restart
service openvpn restart
service cron restart
service ssh restart
service dropbear restart
service squid3 restart
service webmin restart
rm -rf ~/.bash_history && history -c
echo "unset HISTFILE" >> /etc/profile

# info
clear
echo "Autoscript Include:" | tee log-install.txt
echo "===========================================" | tee -a log-install.txt
echo ""  | tee -a log-install.txt
echo "Service"  | tee -a log-install.txt
echo "-------"  | tee -a log-install.txt
echo "OpenSSH  : 22, 143"  | tee -a log-install.txt
echo "Dropbear : 80, 443"  | tee -a log-install.txt
echo "Squid3   : 8080, 3128 (limit to IP SSH)"  | tee -a log-install.txt
echo "OpenVPN  : TCP 1194 (client config : http://$MYIP:81/client.ovpn)"  | tee -a log-install.txt
echo "badvpn   : badvpn-udpgw port 7300"  | tee -a log-install.txt
echo "nginx    : 81"  | tee -a log-install.txt
echo ""  | tee -a log-install.txt
echo "Script"  | tee -a log-install.txt
echo "------"  | tee -a log-install.txt
echo "menu (List commands)"  | tee -a log-install.txt
echo "usernew (Create SSH Account)"  | tee -a log-install.txt
echo "trial-user (Create Trial Account)"  | tee -a log-install.txt
echo "del-user (Delete SSH Account)"  | tee -a log-install.txt
echo "login (Check login user)"  | tee -a log-install.txt
echo "member (List user)"  | tee -a log-install.txt
echo "resvis (Restart Service dropbear, webmin, squid3, openvpn dan ssh)"  | tee -a log-install.txt
echo "reboot (Reboot VPS)"  | tee -a log-install.txt
echo "speedtest (Speedtest VPS)"  | tee -a log-install.txt
echo "about (Information about auto install script)"  | tee -a log-install.txt
echo ""  | tee -a log-install.txt
echo "Another feature"  | tee -a log-install.txt
echo "----------"  | tee -a log-install.txt
echo "Webmin   : http://$MYIP:10000/"  | tee -a log-install.txt
echo "vnstat   : http://$MYIP:81/vnstat/"  | tee -a log-install.txt
echo "Timezone : Asia/Bangkok (GMT +7)"  | tee -a log-install.txt
echo "IPv6     : [off]"  | tee -a log-install.txt
echo ""  | tee -a log-install.txt
echo "Custom Modified by CipherX"  | tee -a log-install.txt
echo "Thanks to Original Creator Kang Arie & Mikodemos & Aiman Amir & YurisshOS"
echo ""  | tee -a log-install.txt
echo "Log Install --> /root/log-install.txt"  | tee -a log-install.txt
echo ""  | tee -a log-install.txt
echo "VPS AUTO REBOOT TIME 12:00 AM AND NOW IT REBOOTING"  | tee -a log-install.txt
echo ""  | tee -a log-install.txt
echo "==========================================="  | tee -a log-install.txt
cd
rm -f /root/debian.sh
reboot