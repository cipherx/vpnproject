#!/bin/bash
#Menu

echo -e "================================ MENU ====================================="
echo -e "* menu       	: Command List"
echo -e "* usernew    	: Create User SSH"
echo -e "* trial-user   : Create User trial"
echo -e "* del-user     : Delete User SSH"
echo -e "* login      	: Check Login User"
echo -e "* member     	: List user"
echo -e "* resvis     	: restart service dropbear, webmin, squid3, openvpn, ssh"
echo -e "* reboot     	: reboot VPS"
echo -e "* speedtest  	: speedtest VPS"
echo -e "* about      	: Information About Script Auto Install"
echo -e "========================================================================="
echo -e ""
